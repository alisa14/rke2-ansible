.PHONY: rke2-pdc rke2-sdc

sdc_inventory := inventory/sdc/hosts.ini
pdc_inventory := inventory/pdc/hosts.ini

rke2-pdc:
	ansible-playbook -i ${pdc_inventory} site.yml -e @secret_vars.yaml
rke2-sdc:
	ansible-playbook -i ${sdc_inventory} site.yml -e @secret_vars.yaml
secret-vars-file:
	op inject -i secret_vars.tmpl -o secret_vars.yaml